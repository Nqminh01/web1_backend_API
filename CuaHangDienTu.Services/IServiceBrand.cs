﻿using CuaHangDienTu.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Services
{
    public interface IServiceBrand
    {
        Task<IEnumerable<Brand>> GetAll();
        Task<Brand> GetById(string id);
        Task Create(Brand brand);
        Task<bool> Update(string id, Brand brand);
        Task<bool> Delete(string id);
    }
}
