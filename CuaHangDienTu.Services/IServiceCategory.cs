﻿using CuaHangDienTu.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Services
{
    public interface IServiceCategory
    {
        Task<IEnumerable<Category>> GetAll();
        Task<Category> GetById(string id);
        Task Create(Category Category);
        Task<bool> Update(string id, Category Category);
        Task<bool> Delete(string id);
    }
}
