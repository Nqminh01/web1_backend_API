﻿using CuaHangDienTu.Domain.Models;
using CuaHangDienTu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Services.Implement
{
    public class ServiceCategory : IServiceCategory
    {
        private readonly IRepositoryCategory<Category> _repository;
        public ServiceCategory(IRepositoryCategory<Category> repository)
        {
            _repository = repository;
        }

        public async Task Create(Category category)
        {
            await _repository.Create(category);
        }

        public async Task<bool> Delete(string id)
        {
            var category = await _repository.GetById(id);
            if (category != null)
            {
                await _repository.Delete(category);
            }
            return true;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<Category> GetById(string id)
        {
            return await _repository.GetById(id);
        }

        public async Task<bool> Update(string id, Category category)
        {
            var category1 = await _repository.GetById(id);
            if (category1 != null)
            {
                category = category1;
                category.CategoryId = category1.CategoryId;
                await _repository.Update(category);
            }
            return true;
        }
    }
}
