﻿using CuaHangDienTu.Domain.Models;
using CuaHangDienTu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Services.Implement
{
    public class ServiceAccount : IServiceAccount
    {
        private readonly IRepositoryAccount<Account> _repository;
        public ServiceAccount(IRepositoryAccount<Account> repository)
        {
            _repository = repository;
        }
        public async Task Create(Account account)
        {
            await _repository.Create(account);
        }

        public async Task<bool> Delete(string id)
        {
            var account = await _repository.GetById(id);
            if (account != null)
            {
                await _repository.Delete(account);
            }
            return true;
        }

        public async Task<IEnumerable<Account>> Filter(bool role)
        {
            return await _repository.Filter(role);
        }

        public async Task<IEnumerable<Account>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<Account> GetById(string id)
        {
            return await _repository.GetById(id);
        }

        public async Task<IEnumerable<Account>> Search(string keyword)
        {
            return await _repository.Search(keyword);
        }

        public async Task<bool> Update(string id,Account account)
        {
            var acc = await _repository.GetById(id);
            if (acc != null)
            {
                account = acc;
                account.UserName = acc.UserName;
                await _repository.Update(account);
            }
            return true;
        }
    }
}
