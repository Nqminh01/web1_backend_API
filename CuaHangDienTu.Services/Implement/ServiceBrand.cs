﻿using CuaHangDienTu.Domain.Models;
using CuaHangDienTu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Services.Implement
{
    public class ServiceBrand : IServiceBrand
    {
        private readonly IRepositoryBrand<Brand> _repository;
        public ServiceBrand(IRepositoryBrand<Brand> repository)
        {
            _repository = repository;
        }

        public async Task Create(Brand brand)
        {
            await _repository.Create(brand);
        }

        public async Task<bool> Delete(string id)
        {
            var brand = await _repository.GetById(id);
            if (brand != null)
            {
                await _repository.Delete(brand);
            }
            return true;
        }

        public async Task<IEnumerable<Brand>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<Brand> GetById(string id)
        {
            return await _repository.GetById(id);
        }

        public async Task<bool> Update(string id, Brand brand)
        {
            var brand1 = await _repository.GetById(id);
            if (brand1 != null)
            {
                brand = brand1;
                brand.BrandId = brand1.BrandId;
                await _repository.Update(brand);
            }
            return true;
        }
    }
}
