﻿using CuaHangDienTu.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Services
{
    public interface IServiceAccount
    {
        Task<IEnumerable<Account>> GetAll();
        Task<Account> GetById(string id);
        Task Create(Account account);
        Task<bool> Update(string id, Account account);
        Task<bool> Delete(string id);
        Task<IEnumerable<Account>> Filter(bool role);
        Task<IEnumerable<Account>> Search(string keyword);
    }
}
