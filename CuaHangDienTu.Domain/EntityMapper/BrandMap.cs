﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class BrandMap : IEntityTypeConfiguration<Brand>
    {
        void IEntityTypeConfiguration<Brand>.Configure(EntityTypeBuilder<Brand> builder)
        {
            builder.ToTable("Brand")
               .HasKey(x => x.BrandId)
               .HasName("pk_brandid");
            builder.Property(x => x.BrandId)
                .HasColumnName("BrandId")
                .HasColumnType("NVARCHAR(10)");
            builder.Property(x => x.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(100)");
            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(250)");
        }
    }
}
