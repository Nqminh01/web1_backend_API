﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class CategoryMap : IEntityTypeConfiguration<Category>
    {
        void IEntityTypeConfiguration<Category>.Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Category")
                .HasKey(x => x.CategoryId)
                .HasName("pk_categoryid");
            builder.Property(x => x.CategoryId)
                .IsRequired()
                .HasColumnName("CategoryId")
                .HasColumnType("NVARCHAR(10)");
            builder.Property(x => x.Name)
                .IsRequired()
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(100)");
            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(250)");
        }
    }
}
