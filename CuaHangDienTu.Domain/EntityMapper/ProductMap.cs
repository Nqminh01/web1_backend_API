﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        void IEntityTypeConfiguration<Product>.Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Product")
               .HasKey(x => x.ProductId)
               .HasName("pk_productid");

            builder.Property(x => x.ProductId)
                .IsRequired()
                .HasColumnName("ProductId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.CategoryId)
                .IsRequired()
                .HasColumnName("CategoryId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.BrandId)
                .IsRequired()
                .HasColumnName("BrandId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.Name)
                .HasColumnName("Name")
                .HasColumnType("NVARCHAR(100)");

            builder.Property(x => x.Image)
                .HasColumnName("Image")
                .HasColumnType("text");

            builder.Property(x => x.Price)
                .HasColumnName("Price")
                .HasColumnType("INT");

            builder.Property(x => x.Quantity)
                .HasColumnName("Quantity")
                .HasColumnType("INT");

            builder.Property( x=> x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(500)");

            builder.Property( x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("BIT");

            builder.HasOne(c => c.Category)
                .WithMany(p => p.Products)
                .HasForeignKey(c => c.CategoryId)
                .HasConstraintName("fk_product_category");

            builder.HasOne(c => c.Brand)
                .WithMany(p => p.Products)
                .HasForeignKey(c => c.BrandId)
                .HasConstraintName("fk_product_brand");
        }
    }
}
