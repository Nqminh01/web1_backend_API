﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class AccountMap : IEntityTypeConfiguration<Account>
    {
        void IEntityTypeConfiguration<Account>.Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("Account")
                .HasKey(x => x.UserName)
                .HasName("pk_accountid");
            builder.Property(x => x.UserName)
                .IsRequired()
                .HasColumnName("UserName")
                .HasColumnType("NVARCHAR(30)");
            builder.Property(x => x.Email)
                .IsRequired()
                .HasColumnName("Email")
                .HasColumnType("NVARCHAR(50)");
            builder.Property(x => x.Password)
                .IsRequired()
                .HasColumnName("Password")
                .HasColumnType("NVARCHAR(100)");
            builder.Property(x => x.Phone)
                .HasColumnName("Phone")
                .HasColumnType("NVARCHAR(11)");
            builder.Property(x => x.Address)
                .HasColumnName("Address")
                .HasColumnType("NVARCHAR(200)");
            builder.Property(x => x.Role)
                .IsRequired()
                .HasColumnName("Role")
                .HasColumnType("BIT");
        }
    }
}
