﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class DetailPaymentMap : IEntityTypeConfiguration<DetailPayment>
    {
        void IEntityTypeConfiguration<DetailPayment>.Configure(EntityTypeBuilder<DetailPayment> builder)
        {
            builder.ToTable("DetailPayment")
              .HasKey(x => new { x.DetailPaymentId, x.PaymentId })
              .HasName("pk_detailpaymentid");

            builder.Property(x => x.DetailPaymentId)
                .IsRequired()
                .HasColumnName("DetailPaymentId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.PaymentId)
                .IsRequired()
                .HasColumnName("PaymentId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.UserName)
                .IsRequired()
                .HasColumnName("UserName")
                .HasColumnType("NVARCHAR(30)");

            builder.Property(x => x.ProductId)
                .IsRequired()
                .HasColumnName("ProductId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.Quantity)
                .IsRequired()
                .HasColumnName("Quantity")
                .HasColumnType("INT");

            builder.Property(x => x.Status)
                .IsRequired()
                .HasColumnName("Status")
                .HasColumnType("BIT");

            builder.HasOne(c => c.Payment)
                .WithMany(p => p.DetailPayments)
                .HasForeignKey(c => c.PaymentId)
                .HasConstraintName("fk_dtpayment_payment");

            builder.HasOne(c => c.Account)
                .WithMany(p => p.DetailPayments)
                .HasForeignKey(c => c.UserName)
                .HasConstraintName("fk_dtpayment_account");

            builder.HasOne(c => c.Product)
                 .WithMany(p => p.DetailPayments)
                 .HasForeignKey(c => c.ProductId)
                 .HasConstraintName("fk_dtpayment_product");

        }
    }
}
