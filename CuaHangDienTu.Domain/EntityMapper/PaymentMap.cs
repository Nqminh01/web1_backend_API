﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class PaymentMap : IEntityTypeConfiguration<Payment>
    {
        void IEntityTypeConfiguration<Payment>.Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.ToTable("Payment")
              .HasKey(x => x.PaymentId)
              .HasName("pk_paymentid");

            builder.Property(x => x.PaymentId)
                .IsRequired()
                .HasColumnName("PaymentId")
                .HasColumnType("NVARCHAR(10)");

            builder.Property(x => x.Receiver)
                .HasColumnName("Receiver")
                .HasColumnType("NVARCHAR(100)")
                .IsRequired();

            builder.Property(x => x.Address)
                .HasColumnName("Address")
                .HasColumnType("NVARCHAR(250)")
                .IsRequired();

            builder.Property(x => x.Phone)
                .HasColumnName("Phone")
                .HasColumnType("NVARCHAR(11)")
                .IsRequired();

            builder.Property(x => x.DateTime)
                .HasColumnName("DateTime")
                .HasColumnType("DATETIME")
                .IsRequired();

            builder.Property(x => x.TotalFee)
                .HasColumnName("TotalFee")
                .HasColumnType("INT");

            builder.Property(x => x.Status)
                .HasColumnName("Status")
                .HasColumnType("INT");

            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(500)");
        }
    }
}
