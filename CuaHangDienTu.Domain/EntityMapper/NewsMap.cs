﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.EntityMapper
{
    public class NewsMap : IEntityTypeConfiguration<News>
    {
        void IEntityTypeConfiguration<News>.Configure(EntityTypeBuilder<News> builder)
        {
            builder.ToTable("News")
               .HasKey(x => x.NewsId)
               .HasName("pk_newsid");
            builder.Property(x => x.NewsId)
                .IsRequired()
                .HasColumnName("NewsId")
                .HasColumnType("NVARCHAR(10)");
            builder.Property(x => x.NewsName)
                .IsRequired()
                .HasColumnName("NewsName")
                .HasColumnType("NVARCHAR(150)");
            builder.Property(x => x.Image)
                .HasColumnName("Image")
                .HasColumnType("text");
            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("NVARCHAR(500)");
            builder.Property(x => x.DateTime)
                .HasColumnName("DateTime")
                .HasColumnType("DateTime");
        }
    }
}
