﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.Models
{
    public class Payment
    {
        public string PaymentId { get; set; }
        public string Receiver { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int TotalFee { get; set; }
        public DateTime DateTime { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public ICollection<DetailPayment> DetailPayments { get; set; }
    }
}
