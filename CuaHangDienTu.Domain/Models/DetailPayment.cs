﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.Models
{
    public class DetailPayment
    {
        public string DetailPaymentId { get; set; }
        public string PaymentId { get; set; }
        public string UserName { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set;}
        public bool Status { get; set; }
        public Payment Payment { get; set; }
        public Product Product { get; set; }
        public Account Account { get; set; }
    }
}
