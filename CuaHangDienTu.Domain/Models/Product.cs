﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.Models
{
    public class Product
    {
        public string ProductId { get; set; }
        public string CategoryId { get; set; }
        public string BrandId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public bool Status { get; set; }
        public Category Category { get; set; }
        public Brand Brand { get; set; }
        public ICollection<DetailPayment> DetailPayments { get; set; }
    }
}
