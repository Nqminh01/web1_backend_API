﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Domain.Models
{
    public class News
    {
        public string NewsId { get; set; }
        public string NewsName { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public DateTime DateTime { get; set; }
    }
}
