﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Repository
{
    public interface IRepositoryBrand<T> : IRepository<T> where T : class
    {
    }
}
