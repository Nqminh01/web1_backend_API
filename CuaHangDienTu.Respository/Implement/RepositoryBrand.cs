﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Repository.Implement
{
    public class RepositoryBrand<Brand> : IRepositoryBrand<Brand> where Brand : class
    {
        private readonly CuaHangDienTuDBContext _context;
        private DbSet<Brand> entities;
        public RepositoryBrand(CuaHangDienTuDBContext context)
        {
            _context = context;
            entities = _context.Set<Brand>();
        }
        public async Task<Brand> Create(Brand entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            await entities.AddAsync(entity);

            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<Brand> Delete(Brand entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<Brand>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<Brand> GetById(string id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<Brand> Update(Brand entity)
        {
            if (entity == null)
            {
                throw new ArgumentException(nameof(entity));
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
