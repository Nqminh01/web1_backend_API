﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Repository.Implement
{
    public class RepositoryCategory<Category> : IRepositoryCategory<Category> where Category : class
    {
        private readonly CuaHangDienTuDBContext _context;
        private DbSet<Category> entities;
        public RepositoryCategory(CuaHangDienTuDBContext context)
        {
            _context = context;
            entities = _context.Set<Category>();
        }
        public async Task<Category> Create(Category entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            await entities.AddAsync(entity);

            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<Category> Delete(Category entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<Category>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<Category> GetById(string id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<Category> Update(Category entity)
        {
            if (entity == null)
            {
                throw new ArgumentException(nameof(entity));
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
