﻿using CuaHangDienTu.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Repository
{
    public class RepositoryAccount<Account> : IRepositoryAccount<Account> where Account : class
    {
        private readonly CuaHangDienTuDBContext _context;
        private DbSet<Account> entities;
        public RepositoryAccount(CuaHangDienTuDBContext context)
        {
            _context = context;
            entities = _context.Set<Account>();
        }

        public async Task<Account> Create(Account entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            await entities.AddAsync(entity);

            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<Account> Delete(Account entity)
        {
            entities.Remove(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<IEnumerable<Account>> Filter(bool role)
        {
            return (IEnumerable<Account>)await _context.Account.Where(a => a.Role == role).ToListAsync();
        }

        public async Task<IEnumerable<Account>> GetAll()
        {
            return await entities.ToListAsync();
        }

        public async Task<Account> GetById(string id)
        {
            return await entities.FindAsync(id);
        }

        public async Task<IEnumerable<Account>> Search(string keyword)
        {
             var listSearch = _context.Account.Where(a => a.UserName.Contains(keyword)).ToListAsync();
            return (IEnumerable<Account>) await listSearch;
        }

        public async Task<Account> Update(Account entity)
        {
            if (entity == null)
            {
                throw new ArgumentException(nameof(entity));
            }
            entities.Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }
    }
}
