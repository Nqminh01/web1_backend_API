﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuaHangDienTu.Repository
{
    public interface IRepositoryAccount<T> : IRepository<T> where T : class
    {
        Task<IEnumerable<T>> Filter(bool role);
        Task<IEnumerable<T>> Search(string keyword);
    }
}
