﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CuaHangDienTu.Repository.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    UserName = table.Column<string>(type: "NVARCHAR(30)", nullable: false),
                    Email = table.Column<string>(type: "NVARCHAR(50)", nullable: false),
                    Password = table.Column<string>(type: "NVARCHAR(100)", nullable: false),
                    Phone = table.Column<string>(type: "NVARCHAR(11)", nullable: true),
                    Address = table.Column<string>(type: "NVARCHAR(200)", nullable: true),
                    Role = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_accountid", x => x.UserName);
                });

            migrationBuilder.CreateTable(
                name: "Brand",
                columns: table => new
                {
                    BrandId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    Name = table.Column<string>(type: "NVARCHAR(100)", nullable: false),
                    Description = table.Column<string>(type: "NVARCHAR(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_brandid", x => x.BrandId);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    CategoryId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    Name = table.Column<string>(type: "NVARCHAR(100)", nullable: false),
                    Description = table.Column<string>(type: "NVARCHAR(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_categoryid", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    NewsId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    NewsName = table.Column<string>(type: "NVARCHAR(150)", nullable: false),
                    Image = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    DateTime = table.Column<DateTime>(type: "DateTime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_newsid", x => x.NewsId);
                });

            migrationBuilder.CreateTable(
                name: "Payment",
                columns: table => new
                {
                    PaymentId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    Receiver = table.Column<string>(type: "NVARCHAR(100)", nullable: false),
                    Phone = table.Column<string>(type: "NVARCHAR(11)", nullable: false),
                    Address = table.Column<string>(type: "NVARCHAR(250)", nullable: false),
                    TotalFee = table.Column<int>(type: "INT", nullable: false),
                    DateTime = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    Description = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Status = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_paymentid", x => x.PaymentId);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    CategoryId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    BrandId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    Name = table.Column<string>(type: "NVARCHAR(100)", nullable: true),
                    Image = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    Price = table.Column<int>(type: "INT", nullable: false),
                    Quantity = table.Column<int>(type: "INT", nullable: false),
                    Status = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_productid", x => x.ProductId);
                    table.ForeignKey(
                        name: "fk_product_brand",
                        column: x => x.BrandId,
                        principalTable: "Brand",
                        principalColumn: "BrandId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_product_category",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DetailPayment",
                columns: table => new
                {
                    DetailPaymentId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    PaymentId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    UserName = table.Column<string>(type: "NVARCHAR(30)", nullable: false),
                    ProductId = table.Column<string>(type: "NVARCHAR(10)", nullable: false),
                    Quantity = table.Column<int>(type: "INT", nullable: false),
                    Status = table.Column<bool>(type: "BIT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_detailpaymentid", x => new { x.DetailPaymentId, x.PaymentId });
                    table.ForeignKey(
                        name: "fk_dtpayment_account",
                        column: x => x.UserName,
                        principalTable: "Account",
                        principalColumn: "UserName",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_dtpayment_payment",
                        column: x => x.PaymentId,
                        principalTable: "Payment",
                        principalColumn: "PaymentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_dtpayment_product",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DetailPayment_PaymentId",
                table: "DetailPayment",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_DetailPayment_ProductId",
                table: "DetailPayment",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_DetailPayment_UserName",
                table: "DetailPayment",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_Product_BrandId",
                table: "Product",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryId",
                table: "Product",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DetailPayment");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Payment");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Brand");

            migrationBuilder.DropTable(
                name: "Category");
        }
    }
}
